extends ImmediateGeometry

export(int) var _max_points = 1000
export(Color) var _color = Color.yellow setget _set_color
export(bool) var _collect = false
export(bool) var _draw = true

onready var _points = Arrays.RoundRobin._Vector3.new(_max_points)


func _ready():
	var m = SpatialMaterial.new()
	m.flags_unshaded = true
	m.flags_use_point_size = true
	
	m.albedo_color = _color
	material_override = m

	build_spiral(4)
	
func build_circle(count := 1, steps := 100):
	var delta = TAU / steps

	for i in steps * count:
		var point = Vector3(cos(i * delta), 0, sin(i * delta))
		_points.add(point)

func build_spiral(count := 1, height := 1.0, steps := 100):
	var delta_tau = TAU / steps
	var delta_height = height / steps
	
	for i in steps * count: 
		var point = Vector3(cos(i * delta_tau), i * delta_height, sin(i * delta_tau))
		print(point)
		_points.add(point)
		
		
func _set_color(color):
	if material_override:
		material_override.albedo_color = color


func _physics_process(delta):	
	if _collect:
		_points.add(to_global(translation))
	
	if _draw:
		clear()	
		begin(Mesh.PRIMITIVE_LINES, null)
		for p in _points:
			add_vertex(to_local(p))
		end()
