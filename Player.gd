extends KinematicBody2D


export var MAX_SPEED := 300
export var ACCELERATION := 100
export var FRICTION := 100

var velocity := Vector2.ZERO

enum { IDLE, RUN } 


func set_state(state := IDLE, input_v := Vector2.ZERO):
	match state:
		RUN:
			if false:
				pass
			elif input_v.x < 0:
				$Sprite.frame = 0
			elif input_v.x > 0:
				$Sprite.frame = 3
			elif input_v.y < 0:
				$Sprite.frame = 1
			else:
				$Sprite.frame = 2
		_:
			$Sprite.frame = 2

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _physics_process(delta):
	var input_x = Input.get_action_strength("ui_right") - Input.get_action_strength("ui_left")
	var input_y = Input.get_action_strength("ui_down") - Input.get_action_strength("ui_up")

	var input_v = Vector2(input_x, input_y)
	
	if input_v != Vector2.ZERO:
		set_state(RUN, input_v)
		velocity = velocity.move_toward(input_v.normalized() * MAX_SPEED, ACCELERATION * delta)
	else:
		set_state(IDLE, input_v)
		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
		

	velocity = move_and_slide(velocity)
#	var slide_count = get_slide_count()
#	for i in slide_count:
#		print(get_slide_collision(i))


#	if input_vector != Vector2.ZERO:
#		animationTree.set("parameters/Idle/blend_position", input_vector)
#		animationTree.set("parameters/Run/blend_position", input_vector)
#		animationTree.set("parameters/Attack/blend_position", input_vector)
#		animationTree.set("parameters/Roll/blend_position", input_vector)
#		animationState.travel("Run")
#		velocity = velocity.move_toward(input_vector.normalized() * MAX_SPEED, ACCELERATION * delta)
#	else:
#		animationState.travel("Idle")
#		velocity = velocity.move_toward(Vector2.ZERO, FRICTION * delta)
