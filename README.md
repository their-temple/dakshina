# Dakshina

Gifts to and from [Their Temple](http://theirtemple.com).  See [Dakshina](https://en.wikipedia.org/wiki/Dakshina)

[Their Temple](http://theirtemple.com) is dedicated to the idea of giving, and all here is presented as a gift.

Of course, [Their Temple](http://theirtemple.com) is also open to the gift of receiving gifts, and to regifting those gifts however best serves.

## Demo

Here is the [Intro to Their Temple](http://theirtemple.com/web/Dakshina/Dakshina.html)

## Intentions

* Design process of categorizing content with licenses
* Document Dakshina sources, such as [FMA](https://freemusicarchive.org/home), [Kenney](https://www.kenney.nl/assets)
* [KenShape for Their Temple](https://kenney.itch.io/kenshape/download/sVfvatnRJdbtr2TfL5Iv1CFnEJOOc8_ZeEyXj9HZ)
* Tracking open source free reusable content as gifts for [Their Temple](http://theirtemple.com) to regift in various forms
* Provide content generation tools to ensure ownership/licensing rights
  - Text model generation for [Logos](https://gitlab.com/their-temple/logos)
  - Audio, image, and avatar view generation for [Acro Avatars](https://gitlab.com/their-temple/acro-avatars)
  - Allowing guests to gift those creations to [Their Temple](http://theirtemple.com), or keep for themselves, as they see fit
* Integration with Godot PCK creation and DLC management tools
* Integration with resource allocation, transfer and tracking tools
  - Proxytopia for givers of other's gifts
  - [Go-Dough-Crypto](https://gitlab.com/their-temple/go-dough-crypto) for exchanges using Their Temple Alms or other crypto currencies
* Receive and give gifts of Their Temple Alms or other crypto currencies using Go-Dough-Crypto

## Attributions

### Audio

- /Sounds/SONNIK [license](https://creativecommons.org/licenses/by-nc-sa/4.0/)
  - [SONNIK - Sonnik.7.12.mp3](https://freemusicarchive.org/music/SONNIK/SONNIK_70/Fjodor_Lavrov_-_Sonnik_70_-_12_Sonnik_712)
  - SONNIK - Sonnik.7.12.* - Transformed by [Their Temple](http://theirtemple.com) with [VLC](https://www.videolan.org/vlc/)

### Fonts

- [Akaya Telivigala](https://fonts.google.com/specimen/Akaya+Telivigala) - [Open Font License](OFL.txt)

### Authors

- [Kenney](https://www.kenney.nl/assets)

### Tools

- [Godot](http://godotengine.org)
- [Tilesetter](https://www.tilesetter.org/)
- [Aseprite](https://www.aseprite.org/)

### Tutorials

- [Heart Beast](https://www.youtube.com/c/uheartbeast/videos)
- [Studio Mini Boss](https://blog.studiominiboss.com/pixelart)
