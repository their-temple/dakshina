class_name Arrays

class RoundRobin:
	
	var _array := []
	var _is_full := false
	var _insert := 0

	var _iter := 0
	
	func _init(size, array = []):
		assert(size > 0)
		_array = array
		_array.resize(size)
		
	func add(p):		
		_array[_insert] = p
		_insert += 1
		if _insert == _array.size():
			_is_full = true
			_insert = 0

		
	func size():
		return _array.size() if _is_full else _insert
		
	func should_continue():
		return _iter < size()
		
	func _iter_init(_arg) -> bool:
		_iter = 0
		
		return should_continue()
	
	func _iter_next(_arg) -> bool:
		_iter += 1
		
		return should_continue()
		
	func _iter_get(_arg):
		return _array[posmod(_insert + _iter if _is_full else _iter, _array.size())]

	class _Vector3:
		extends RoundRobin
		func _init(size).(size, PoolVector3Array()):
			pass
	
	class Int:
		extends RoundRobin
		func _init(size).(size, PoolIntArray()):
			pass
			
