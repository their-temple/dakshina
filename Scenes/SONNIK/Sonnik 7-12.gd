extends AnimationPlayer


signal bass(which)
signal cello(which)
signal violin(which)


# Called when the node enters the scene tree for the first time.
func _ready():
	play("Play")

func bass(which : int):
	emit_signal("bass", which)

func cello(which : int):
	emit_signal("cello", which)
	
func violin(which : int):
	emit_signal("violin", which)

