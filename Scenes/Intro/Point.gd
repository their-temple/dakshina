extends Spatial


# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

func _physics_process(_delta):
	# move_and_slide(velocity)
	var label = $Label
	
	var offset = Vector2(label.rect_size.x/2, 0)

	label.rect_position = get_viewport().get_camera().unproject_position(to_global(translation)) - offset
