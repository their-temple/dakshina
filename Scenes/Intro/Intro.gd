extends Node

export(bool) var goto_their_temple = true

onready var directions = $World/KinematicBody2D/Directions
onready var label = $World/KinematicBody2D/Directions/CenterContainer/Label

func _ready():
	if OS.has_feature("HTML5"):
		label.text = "Click for sound\n" + label.text
		
func _physics_process(delta):
	if Input.is_action_just_pressed("ui_focus_next"):
		directions.visible = not directions.visible

	if Input.is_action_just_pressed("ui_cancel"):
		$AudioStreamPlayer.stop()
		$AudioStreamPlayer3D.stop()
		$AnimationPlayer.advance($AnimationPlayer.current_animation_length * 2)
		$AnimationPlayer.stop()
		directions.visible = false

	if Input.is_action_just_pressed("ui_accept"):
		$World.visible = not $World.visible
		
func _on_AnimationPlayer_animation_finished(anim_name):
	if OS.has_feature("HTML5") and goto_their_temple:
		OS.shell_open("http://theirtemple.com")
